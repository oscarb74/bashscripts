#!/bin/bash
############################################################################
#title           : prueba_email_postfix.sh
#description     : envio de emails
#author          : Óscar Borrás
#email           : oscarborras@iesjulioverne.es
#date            : 29-07-2016
#version         : 1.0
#license         : GNU GPLv3 
############################################################################



############################################################################
#  INSTRUCCIONES
############################################################################
#- Se debe tener instalado el paquete mailutils

############################################################################
#  FALLOS POR CORREGIR
############################################################################
# 

############################################################################
#  FUNCIONES PENDIENTES DE IMPLEMENTAR
############################################################################
#
# - 
# - config envio de email por smtp del julioverne


############################################################################
# LISTA DE ESTADOS DE EXIT:
############################################################################
# 0   - Ok



############################################################################
# Inicialización de variables configurables
############################################################################

DATE_LOG_FORMAT="%Y-%m-%d_%H:%M:%S"
LOG="./prueba_email_postfix.log"	#Fichero log del programa
TTY_SALIDA=`tty` 

FROM_EMAIL=mantenimiento@iesjulioverne.es
#SMSEMAIL=<cell phone number @ sms-gateway>
DESTINATARIO_EMAIL=oscarb74@gmail.com


############################################################################
# BLOQUE FUNCIONES
############################################################################

# Función que envia emails
# Uso: enviar_email <destinatario> <asunto> <msg>
function enviar_email(){
	echo $3 | mail -s $2 $1

}

asunto="asunto de prueba"
msg="mensaje de prueba ..."
enviar_email "$DESTINATARIO_EMAIL" "$asunto" "$msg"
#echo "[ `date +%Y-%m-%d_%H:%M:%S` ] mensaje de prueba." | /usr/bin/mailx -s "asunto." -r $FROM_EMAIL $DESTINATARIO_EMAIL  >> $LOG


function install(){
	#indicamos las opciones a utilizar por el asistente de instalacion de postfix
	debconf-set-selections <<< "postfix postfix/mailname string $HOSTNAME.iesjulioverne.local"
	debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"

	echo "** postfix **" | tee -a $FICH_LOG $ERROR_LOG  && sudo apt-get -y install postfix >> $FICH_LOG 2>> $ERROR_LOG
	echo "** mail **" | tee -a $FICH_LOG $ERROR_LOG  && sudo apt-get -y install mailutils >> $FICH_LOG 2>> $ERROR_LOG

	# falta añadir fichero de config ############# mirar mi blog  ---- En ubuntu no hace falta. Envio por google aunque se va al spam
	#hacer en otro script la config de postfix para que use el smtp del departamento
}
