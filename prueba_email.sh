#!/bin/bash
############################################################################
#title           : prueba_email.sh
#description     : comprobación de envio de emails
#author          : Óscar Borrás
#email           : oscarborras@iesjulioverne.es
#date            : 17-11-2015
#version         : 1.0
#license         : GNU GPLv3 
############################################################################



############################################################################
#  INSTRUCCIONES
############################################################################
#- Se debe tener instalado el paquete heirloom-mailx

############################################################################
#  FALLOS POR CORREGIR
############################################################################
# 

############################################################################
#  FUNCIONES PENDIENTES DE IMPLEMENTAR
############################################################################
#
# - 
# - config envio de email por smtp del julioverne


############################################################################
# LISTA DE ESTADOS DE EXIT:
############################################################################
# 0   - Ok



############################################################################
# Inicialización de variables configurables
############################################################################

DATE_LOG_FORMAT="%Y-%m-%d_%H:%M:%S"
LOG="./prueba_email.log"	#Fichero log del programa
TTY_SALIDA=`tty` 

FROM_EMAIL=mantenimiento@iesjulioverne.es
#SMSEMAIL=<cell phone number @ sms-gateway>
DESTINATARIO_EMAIL=oscarb74@gmail.com

# Función que envia emails
# Uso: enviar_email <destinatario> <asunto> <msg>
enviar_email(){
	#/usr/bin/mailx -s "${SERVERS[$ix]} está de nuevo accesible" -r $FROM_EMAIL $DESTINATARIO_EMAIL
	echo $3 | mailx -v \
		-r "$FROM_EMAIL" \
		-s "$2" \
		-S smtp="mail.iesjulioverne.es:587" \
		-S smtp-use-starttls \
		-S smtp-auth=login \
		-S smtp-auth-user="alertas@iesjulioverne.es" \
		-S smtp-auth-password="scriptMAIL." \
		-S ssl-verify=ignore \
		"$1"
}

asunto="asunto de prueba"
msg="mensaje de prueba ..."
enviar_email "$DESTINATARIO_EMAIL" "$asunto" "$msg"
#echo "[ `date +%Y-%m-%d_%H:%M:%S` ] mensaje de prueba." | /usr/bin/mailx -s "asunto." -r $FROM_EMAIL $DESTINATARIO_EMAIL  >> $LOG
