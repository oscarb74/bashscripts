#!/bin/bash

############################################################################
#title           : hwdetection
#description     : detecta el hw de un pc y comprueba si ha cambiado
#author          : Óscar Borrás
#email           : oscarborras@iesjulioverne.es
#date            : 2016-07-28
#version         : 0.1
#license         : GNU GPLv3 
############################################################################



############################################################################
#  INSTRUCCIONES
############################################################################
# Se debe tener instalado: sqlite3, heirloom-mailx


############################################################################
#  FALLOS POR CORREGIR
############################################################################
#
# - config envio de email por smtp del julioverne

############################################################################
#  FUNCIONES PENDIENTES DE IMPLEMENTAR
############################################################################
# 1.- Automatizar el inicio de este script al arrancar el PC.
#
# - Envio del email que avisa a los profesores (hacerlo instalando servidor email???)
# - Generación de logs para cada proceso realizado
# - Fijar una ruta mas segura para los ficheros de config .cfg
# - Actualizar BBDD sqlite sincronizando con el original o fuente mediante git 


############################################################################
# LISTA DE ESTADOS DE EXIT:
############################################################################
# 0   - Ok

############################################################################
# Inicialización de variables configurables
############################################################################
#Vbles generales
VERSION="0.1"
LOG="hwdetection.log"	#Fichero log del programa
TTY_SALIDA=`tty` #Poner /dev/null si no desea salida en una terminal. La terminal debe existir
FECHA_LOG=`date +"%Y-%m-%d_%H:%M:%S"`;

#Vbles App
MEMORIA="nd"
DISCO="nd"
AVISO=false
PC_NAME=`hostname`

BBDD=$(dirname $0)/hwdetection.db

#Vbles Repo
REPO_NAME="hwdetection" #nombre repo en github

#variables Email
FROM_EMAIL="alertas@iesjulioverne.es"
#SMSEMAIL=<cell phone number @ sms-gateway>
#DESTINATARIO_EMAIL="oscarborras@iesjulioverne.es,mantenimiento@iesjulioverne.es,oscarb74@gmail.com"
DESTINATARIO_EMAIL="oscarb74@gmail.com"
#EMAILS_ALERTA="oscarborras@iesjulioverne.es,mantenimiento@iesjulioverne.es,oscarb74@gmail.com" 
source ./$REPO_NAME.cfg 


############################################################################
# BLOQUE FUNCIONES
############################################################################

# Función que envia email de aviso
# Uso: enviar_email <destinatario> <asunto> <msg>
enviar_email(){
	#/usr/bin/mailx -s "${SERVERS[$ix]} está de nuevo accesible" -r $FROM_EMAIL $DESTINATARIO_EMAIL
	echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Intentando enviar email:" | tee "$TTY_SALIDA" >> $LOG
	echo -e "$3" | mailx \
		-r "$FROM_EMAIL" \
		-s "$2" \
		-S smtp="mail.iesjulioverne.es:587" \
		-S smtp-use-starttls \
		-S smtp-auth=login \
		-S smtp-auth-user="alertas@iesjulioverne.es" \
		-S smtp-auth-password="$EP" \
		-S ssl-verify=ignore \
		"$1"
	if [ $? -eq 0 ]; then
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Email enviado satisfactoriamente." | tee "$TTY_SALIDA" >> $LOG
	else
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Alerta] Error al enviar email." | tee "$TTY_SALIDA" >> $LOG
	fi
}

# Función que obtiene los datos del PC en la BBDD
function obtener_hw_BBDD() {
	local sql="select memoria,disco from hw where pc='$1'"
	local row=`sqlite3 $BBDD "${sql}"`
	echo $row	
}

# Función que averigua el hardware de un pc
# Uso:
function obtener_hw_pc(){
	#memoria
	#memoria_actual=`free -h  | grep -i Mem | awk {'print $2'}`
	MEMORIA=`cat /proc/meminfo | grep -i memtotal | awk {'print $2 " " $3'}`
	echo "Memoria instalada: $MEMORIA" #codigo depuracion

	#disco duro
	DISCO=`lsblk -fm /dev/sda | grep -w sda | awk {'print $3'}`
	echo "Disco instalado: $DISCO" #codigo depuracion
}

# Función que averigua el hardware de un pc
# Uso:
function comprobar_hw_pc(){

	obtener_hw_pc

	datos=$(obtener_hw_BBDD $HOSTNAME)
	memoria_BBDD=`echo $datos | awk -F'|' '{print $1}'`
	disco_BBDD=`echo $datos | awk -F'|' '{print $2}'`
	echo "Memoria en BBDD: $memoria_BBDD" #codigo depuracion	
	echo "Disco en BBDD: $disco_BBDD" #codigo depuracion

	#Si hay modificaciones en el hardware avisar por email. Si falla el email avisar a un equipo de profesor con netcat o similar y registrarlo en el equipo.
	msg="AVISO DE CAMBIO DE HARDWARE"
	msg="$msg\n---------------------------------------------------------------"
	msg="$msg\nPC: $PC_NAME"
	msg="$msg\n---------------------------------------------------------------"
	if [[ $MEMORIA != $memoria_BBDD ]]; then
		msg="$msg\nMemoria actual: $MEMORIA\nMemoria BBDD: $memoria_BBDD"
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Alerta] Memoria actual: $MEMORIA ** Memoria BBDD: $memoria_BBDD" | tee "$TTY_SALIDA" >> $LOG
		#enviar_email $msg
		#echo -e $msg
		AVISO=true
	fi
	
	if [[ $DISCO != $disco_BBDD ]]; then
		msg="$msg\nDisco actual: $DISCO\nDisco BBDD: $disco_BBDD"
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Alerta] Disco actual: $DISCO ** Disco BBDD: $disco_BBDD" | tee "$TTY_SALIDA" >> $LOG
		#enviar_email $msg
		AVISO=true
	fi
	
	if [[ $AVISO == "true" ]]; then
		asunto="Alerta: cambio de Hardware en PC \"$PC_NAME\""
		enviar_email "$DESTINATARIO_EMAIL" "$asunto" "$msg"
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Alerta] ¡¡¡Hardware cambiado!!!" | tee "$TTY_SALIDA" >> $LOG
	else
		echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] No hay cambio de hardware." | tee "$TTY_SALIDA" >> $LOG
	fi

}

# función de ayuda
function ayuda() {
cat << DESCRIPCION_AYUDA
SYNOPSIS
    $0 [-h -v]
DESCRIPCION
    Monitorizar el hardware del equipo
CODIGOS DE RETORNO
    0 Si no hay ningún error
DESCRIPCION_AYUDA
}

function version() {
	echo "$0 versión $VERSION" 
	echo
}


function Main(){
	logger -p user.info -t hwdetection "Iniciado script HWdetection."
	echo "------------------------------------------------------------------------" | tee "$TTY_SALIDA" >> $LOG
	echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Iniciando HWdetection ..." | tee "$TTY_SALIDA" >> $LOG
	
	#buscar_actualizaciones
	
	comprobar_hw_pc
	
	logger -p user.info -t backup_HTPC "script HWdetection Finalizado."
	echo "[ `date +"%Y-%m-%d_%H:%M:%S"` :Info] Finalizado HWdetection." | tee "$TTY_SALIDA" >> $LOG
	echo | tee "$TTY_SALIDA" >> $LOG
}

############################################################################
# FIN BLOQUE FUNCIONES
############################################################################

#para permitir que se cargue el sistema completo hacemos una pausa
#sleep 1m

#if [[ $EUID -ne 0 ]]; then
#	echo "Este script debe ser ejecutado por el usuario root" > $TTY_SALIDA
#	logger -p user.err -t backup_diskusb_IES "Script NO ejecutado como root"
#	exit 1
#fi

if [ $# -gt 0 ]; then
	case $1 in
		"-h"|"--help")
			ayuda
			exit 0 ;;
		"-v"|"--version")
			version
			exit 0 ;;
		*)
			echo "Parámetro '$1' incorrecto."
			ayuda
			exit 0 ;;
	esac
fi

#Llamada a la función Main que inicia el script
Main
