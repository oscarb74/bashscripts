#!/bin/bash

############################################################################
#title           : wol_pc
#description     : encendido de otro pc de nuestra LAN
#author          : Óscar Borrás
#email           : oscarborras@iesjulioverne.es
#date            : 07-10-2015
#version         : 1.0a
#license         : GNU GPLv3 
############################################################################



############################################################################
#  INSTRUCCIONES
############################################################################
#1.- Instalaremos wakeonlan para poder encender el equipo deseado 
#sudo apt-get install wakeonlan 

#2.- Ejecutamos el script al inicio del sistema.
#2.1.- Lo movemos a la carpeta de ejecución de scripts iniciales
#sudo mv wol_pc.sh /etc/init.d

#2.2.- Le otorgamos los permisos
#sudo chmod 755 /etc/init.d/wol_pc.sh

#4.3.- lo iniciamos como servicio a varios niveles gui command etc
#sudo update-rc.d wol_pc.sh defaults

############################################################################
#  FALLOS POR CORREGIR
############################################################################
#
# 

############################################################################
#  FUNCIONES PENDIENTES DE IMPLEMENTAR
############################################################################
#
# - Autoinstalación en el pc que producira el encendido


############################################################################
# LISTA DE ESTADOS DE EXIT:
############################################################################
# 0   - Ok

############################################################################
# Inicialización de variables configurables
############################################################################

IP_WOL="172.16.0.1" #ip endian
#MAC_WOL="00:01:2e:31:58:90"          
MAC_WOL="A0:F3:C1:01:3B:2B"  #eth1
#MAC_WOL="9E:8E:F9:F9:1E:55"  #br0
#MAC_WOL="94:DE:80:42:16:8D"  #eth0
#MAC_WOL="A0:F3:C1:01:3B:35"  #eth2
#MAC_WOL="68:05:CA:15:87:89"  #eth3



#TTY_SALIDA=`tty` #Poner /dev/null si no desea salida en una terminal. La terminal debe existir
TTY_SALIDA="wol_pc.log"

############################################################################
# BLOQUE FUNCIONES
############################################################################


function Main(){
	while true ; do
		pc_wol_status=`comprobar_pc_encendido $IP_WOL`

		if [ $pc_wol_status -eq 1 ]
		then
			encender_pc $MAC_WOL
		else
			echo "[ `date +%Y-%m-%d_%H:%M:%S` :Info] El PC $IP_WOL ya esta encendido. No se intenta arrancar." >> "$TTY_SALIDA"
		fi
		sleep 5m
	done
}	

	
# Función que comprueba si el pc que se le pase por parametro esta encendido
# Uso: comprobar_pc_encendido <IP del pc a comprobar>
# return=0 --> está encendido.
# return=1 --> no está encendido.
comprobar_pc_encendido(){
	echo "[ `date +%Y-%m-%d_%H:%M:%S` :Info] Comprobando si esta encendido el PC $1 ..." >> "$TTY_SALIDA"
#	ping -c 2 $1 > /dev/null | yad --button=gtk-cancel:1 --title="Probando conectividad." --text="Intentando conectar con $IP..." --progress --pulsate --auto-close --auto-kill
	
	ping -c 2 $1 > /dev/null
	echo $?
}

# Función que arranca el pc que se le pase por parametro
# Uso: encender_pc <MAC del pc a arrancar>
encender_pc(){
	echo "[ `date +%Y-%m-%d_%H:%M:%S` :Info] PC $1 esta apagado. Intentando arrancar ..." >> "$TTY_SALIDA"
	wakeonlan $1 >> "$TTY_SALIDA"
	echo "[ `date +%Y-%m-%d_%H:%M:%S` :Info] El PC debe haberse iniciado." >> "$TTY_SALIDA"
}



############################################################################
# FIN BLOQUE FUNCIONES
############################################################################

Main


